use std::fs;
use std::fs::File;
use std::io::{BufReader, Read, Write};
use std::path::{Path, PathBuf};
use std::str::FromStr;

use actix_cors::Cors;
use actix_files::NamedFile;
use actix_web::{get, post, web, App, HttpRequest, HttpResponse, HttpServer, Responder, ResponseError};
use actix_web::http::StatusCode;
use actix_web::middleware::Logger;
use actix_web::web::Data;
use actix_web_httpauth::extractors::bearer::BearerAuth;
use actix_web_httpauth::middleware::HttpAuthentication;
use autometrics::autometrics;
use chrono::Utc;
use futures::{StreamExt};
use futures::stream::TryStreamExt;
use jsonwebtoken::{decode, DecodingKey, Validation};
use mongodb::Client;
use mongodb::bson::{doc};
use mongodb::options::ClientOptions;
use lazy_static::lazy_static;
use log::info;
use serde::{Deserialize, Serialize};
use simplelog::{CombinedLogger, Config, TermLogger, LevelFilter, WriteLogger, TerminalMode, ColorChoice, SharedLogger};
use toml;
use uuid::Uuid;
use oauth2::{AuthUrl, ClientId, ClientSecret,  DeviceAuthorizationUrl, TokenUrl};
use oauth2::basic::BasicClient;
use rand::RngCore;
use rand::rngs::OsRng;

mod app_data;
use app_data::AppData;

mod app_state;
use app_state::AppState;

mod file_record;
use file_record::FileRecord;

mod configuration;
use configuration::GlobalConfiguration;
use crate::authorization::Claims;

mod authorization;

lazy_static! {
    static ref BASE_CONFIGURATION_PATH: &'static Path = Path::new(if cfg!(debug_assertions) { "." } else { "/etc/melody/" });
    static ref CONFIGURATION_FILE_PATH: PathBuf = BASE_CONFIGURATION_PATH.join("server.conf.toml");
    static ref SECRET_FILE_PATH: PathBuf = BASE_CONFIGURATION_PATH.join("secret");
    static ref GLOBAL_CONFIGURATION: GlobalConfiguration = read_configuration_file();
    static ref JWT_SECRET: Vec<u8> = get_encoding_key();
}

fn get_encoding_key() -> Vec<u8> {
    if !SECRET_FILE_PATH.exists() {
        let mut secret = [0u8; 256];
        OsRng.fill_bytes(&mut secret);
        fs::write(SECRET_FILE_PATH.clone(), secret).unwrap();
    }
    fs::read(SECRET_FILE_PATH.clone()).unwrap()
}

fn read_configuration_file() -> GlobalConfiguration {
    let configuration: GlobalConfiguration;
    if CONFIGURATION_FILE_PATH.exists() {
        let file = File::open(CONFIGURATION_FILE_PATH.clone()).unwrap();
        let mut reader = BufReader::new(file);
        let mut contents = String::new();
        reader.read_to_string(&mut contents).unwrap();
        configuration = toml::from_str(&contents).unwrap();
    } else {
        configuration = Default::default();
    }

    return configuration;
}

#[get("/metrics")]
async fn metrics() -> impl Responder {
    match autometrics::encode_global_metrics() {
        Ok(metrics) => HttpResponse::Ok().body(metrics),
        Err(err) => HttpResponse::InternalServerError().body(err.to_string())
    }
}

#[autometrics]
#[get("/download/{file:.*}")]
async fn get_file(request: HttpRequest, app_data: web::Data<AppData>) -> impl Responder {
    let path_str = request.match_info().query("file");
    let collection = app_data.database.collection::<FileRecord>("files");
    let mut matches: Vec<FileRecord> = collection
        .find(doc! { "name": path_str }, None).await.unwrap()
        .try_collect().await.unwrap();
    matches.sort_unstable_by_key(|file_record| file_record.upload_time);
    return NamedFile::open(GLOBAL_CONFIGURATION.storage_path.join(&matches.last().unwrap().stored_as));
}

#[autometrics]
fn write_uploaded_file_to_storage(uploaded_file: Vec<u8>, stored_name: String) -> std::io::Result<()> {
    let full_path = GLOBAL_CONFIGURATION.storage_path.join(stored_name);
    let mut file_to_write = File::create(&full_path)?;
    file_to_write.write_all(&uploaded_file)
}

// TODO: #1 support score-timewise
#[derive(Debug, Deserialize)]
#[serde(rename = "score-partwise")]
struct ScorePartwise {
    identification: Option<Identification>,
}

#[derive(Debug, Deserialize)]
struct Identification {
    rights: Option<String>
}

fn validate_musicxml_file(path: &str, file_bytes: &Vec<u8>) -> Result<(), HttpResponse> {
    let contents = match String::from_utf8(file_bytes.clone()) {
        Ok(contents) => contents,
        Err(error) => return Err(HttpResponse::BadRequest()
            .body(
                format!(
                    "Failed to parse uploaded file ({}) bytes to string with error: {}",
                    path,
                    error))),
    };

    let deserialized: ScorePartwise = match serde_xml_rs::from_str(contents.as_str()) {
        Ok(score) => score,
        Err(error) => return Err(HttpResponse::BadRequest()
            .body(
                format!(
                    "Failed to parse uploaded file ({}) string to XML with error: {}",
                    path,
                    error))),
    };

    match deserialized.identification {
        Some(identification) => match identification.rights {
            Some(rights) => {
                if rights.to_lowercase() != "creative commons zero" {
                    return Err(HttpResponse::BadRequest()
                        .body("License must be creative commons zero"));
                }
            },
            None => return Err(HttpResponse::BadRequest()
                .body("musicXML file identification tag must contain rights tag")),
        },
        None => return Err(HttpResponse::BadRequest()
            .body("musicXML file must contain identification tag"))
    }

    Ok(())
}

#[post("/upload/{file:.*}")]
async fn post_file(request: HttpRequest, credentials: BearerAuth, mut body: actix_multipart::Multipart, app_data: web::Data<AppData>) -> impl Responder {
    let path = request.match_info().query("file");

    if !(path.ends_with(".musicxml") || path.ends_with(".json")) {
        return HttpResponse::BadRequest().body("File extension must be .musicxml or .json");
    }

    let decode_result = decode::<Claims>(
        credentials.token(),
        &DecodingKey::from_secret(JWT_SECRET.as_ref()),
        &Validation::default())
        .unwrap();

    if path == "" {
        return HttpResponse::BadRequest().status(StatusCode::BAD_REQUEST).body("Filename must be provided in URL");
    }

    let stored_name = Uuid::new_v4().to_string();

    let mut uploaded_chunks = Vec::new();

    while let Some(Ok(mut field)) = body.next().await {
        while let Some(chunk) = field.next().await {
            uploaded_chunks.push(chunk.unwrap());
        }
    }

    let stored_name_clone = stored_name.clone();
    let file_bytes = uploaded_chunks.concat();

    if path.ends_with(".musicxml") {
        if let Err(response) = validate_musicxml_file(path, &file_bytes) {
            return response
        }
    }

    let write_result = web::block(move || write_uploaded_file_to_storage(file_bytes, stored_name_clone)).await;
    if let Err(error) = write_result {
        return error.error_response();
    }

    let collection = app_data.database.collection::<FileRecord>("files");
    let file_record = FileRecord::new(
        decode_result.claims.username,
        path.to_string(),
        stored_name,
        Utc::now().naive_utc());
    collection.insert_one(file_record, None).await.unwrap();
    return HttpResponse::Accepted().body("File added to repository");
}

#[derive(Serialize)]
pub struct Books {
    pub book_names: Vec<String>,
}

impl Books {
    pub fn new(book_names: Vec<String>) -> Self {
        Self {
            book_names,
        }
    }
}

#[autometrics]
#[get("/books")]
async fn books(app_data: web::Data<AppData>) -> actix_web::Result<impl Responder> {
    let collection = app_data.database.collection::<FileRecord>("files");

    let unique_records = collection.distinct("name", Some(doc!{"name": { "$regex": ".json$" } }), None).await.unwrap();

    let book_names: Vec<String> = unique_records
        .iter()
        .map(| item | String::from_str(item.as_str().unwrap()).unwrap())
        .collect();

    let books = Books::new(book_names);
    Ok(web::Json(books))
}

#[get("/")]
async fn index() -> impl Responder {
    return NamedFile::open("../frontend/dist/index.html");
}

fn get_loggers_from_config() -> Vec<Box<dyn SharedLogger>> {
    let mut output: Vec<Box<dyn SharedLogger>> = Vec::new();

    for logging_type in &GLOBAL_CONFIGURATION.logging.types {
        output.push(match logging_type.output.as_str() {
            "terminal" => TermLogger::new(LevelFilter::Info, Config::default(), TerminalMode::Mixed, ColorChoice::Auto),
            "file" => WriteLogger::new(LevelFilter::Info, Config::default(), File::create("backend.log").unwrap()),
            _ => panic!("logging configuration is incorrect"),
        });
    }

    return output;
}

fn check_configuration() {
    info!("Checking configuration");
    if GLOBAL_CONFIGURATION.storage_path.exists() {
        info!("{}",
            format!(
            "File storage directory {} exists",
                GLOBAL_CONFIGURATION.storage_path.to_str().unwrap()));
    } else {
        info!("{}",
            format!(
            "File storage directory {} does not exist, creating...",
            GLOBAL_CONFIGURATION.storage_path.to_str().unwrap()));
        fs::create_dir_all(&GLOBAL_CONFIGURATION.storage_path).unwrap();
        info!("{}",
            format!(
            "{} Successfully created",
            GLOBAL_CONFIGURATION.storage_path.to_str().unwrap()));
    }
}

#[tokio::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "debug");
    CombinedLogger::init(get_loggers_from_config())
        .unwrap();

    check_configuration();

    let _ = autometrics::global_metrics_exporter();

    info!("{}", format!("Melody server starting on port {}", GLOBAL_CONFIGURATION.network_port));
    let database_options = ClientOptions::parse(GLOBAL_CONFIGURATION.database.url.clone()).await.unwrap();
    HttpServer::new(move || {
        let database = Client::with_options(database_options.clone()).unwrap().database("music_book");
        let app_data = web::Data::new(AppData::new(database));
        let authentication = HttpAuthentication::bearer(authorization::token_validator);
        let cors = Cors::default()
            .allowed_origin("https://github.com")
            .allow_any_method()
            .allow_any_header()
            .supports_credentials();
        App::new()
            .wrap(Logger::default())
            .app_data(Data::new(AppState::new(
                BasicClient::new(
                    ClientId::new(GLOBAL_CONFIGURATION.oauth_client_id.clone()),
                    Some(ClientSecret::new(GLOBAL_CONFIGURATION.oauth_client_secret.clone())),
                    AuthUrl::new("https://github.com/login/oauth/authorize".to_string()).unwrap(),
                    Some(TokenUrl::new("https://github.com/oauth/token".to_string()).unwrap()))
                    .set_device_authorization_url(DeviceAuthorizationUrl::new("https://github.com/login/device/code".to_string()).unwrap()),
                "https://github.com/api/v4".to_string())))
            .app_data(app_data)
            .app_data(actix_web_httpauth::extractors::bearer::Config::default())
            .service(web::scope("/restricted").wrap(authentication).service(post_file))
            .wrap(cors)
            .service(authorization::routes::login)
            .service(get_file)
            .service(books)
            .service(authorization::routes::auth)
            .service(metrics)
    })
        .bind(("0.0.0.0", GLOBAL_CONFIGURATION.network_port))?
        .run()
        .await
}
