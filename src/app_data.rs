use mongodb::Database;

#[derive(Debug, Clone)]
pub struct AppData {
    pub database: Database,
}

impl AppData {
    pub fn new(database: Database) -> Self {
        Self {
            database
        }
    }
}