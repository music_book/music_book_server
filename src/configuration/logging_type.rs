use serde::Deserialize;

#[derive(Deserialize)]
pub struct LoggingType {
    pub output: String,
    pub level: String,
}

impl LoggingType {
    pub fn new(output: String, level: String) -> Self {
        Self {
            output,
            level,
        }
    }
}
