use serde::Deserialize;

use super::LoggingType;

#[derive(Deserialize)]
pub struct Logging {
    pub types: Vec<LoggingType>,
}

impl Logging {
    fn new(types: Vec<LoggingType>) -> Self {
        Self {
            types,
        }
    }
}

impl Default for Logging {
    fn default() -> Self {
        Self::new(vec![LoggingType::new("terminal".to_string(), "debug".to_string())])
    }
}
