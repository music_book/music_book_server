use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct LoginInfo {
    pub login: String,
}
